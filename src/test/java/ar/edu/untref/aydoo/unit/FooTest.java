package ar.edu.untref.aydoo.unit;


import ar.edu.untref.aydoo.Foo;

import org.junit.Assert;
import org.junit.Test;


public class FooTest 
{
    @Test
    public void doFooShouldReturnFoo()
    {
        Foo foo = new Foo();
        String result = foo.doFoo();
        Assert.assertEquals("foo", result);
    }
}
