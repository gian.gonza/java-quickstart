package ar.edu.untref.aydoo.unit;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class StringTest {

	@Test
	void testLenghtString() {
		//arrange
		String	prueba = new String("Hola Mundo");
		
		//Act
		int longitud = prueba.length();
		
		//Assert
		assertEquals(longitud,prueba.length());
		
	}
	@Test
	public void startWithDevuelveTrueCuandoLaPrimeraLetraConincide() {
		
				//arrange
				String	prueba = new String("Hola Mundo");
				
				Boolean resultado = prueba.startsWith("H");
				
				assertTrue(resultado);
		
	}
	
	@Test

	public void stringCompae() {
		
		//arrange
		String	prueba = new String("Hola Mundo");
		String prueba2 = new String("Hola Mundo");
		
		//Act
		Boolean result = prueba.equals(prueba2);
		//Act
		assertTrue(result);
		
		
		
	}
}
